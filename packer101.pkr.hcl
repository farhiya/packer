packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "base_values" {
    ami_name        =   "farhiya-ami"
    instance_type   =   "t2.micro"
    region          =   "eu-west-1"
    profile         =   "Academy"
    source_ami      =   "ami-0bf84c42e04519c85"
    vpc_id          =   "vpc-4bb64132"
    ssh_username    =   "ec2-user"
}

build {

  # select source to build ami
  sources = ["source.amazon-ebs.base_values"]

  # select a provisioner to configure a machine
  provisioner "shell" {
      script = "./httpd_simple.sh"
  }


}